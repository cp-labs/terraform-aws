output "core_vpc_id" {
  value = module.core.out_vpc_id
}

output "core_vpc_cidr" {
  value = module.core.out_vpc_cidr
}

output "vpc_details" {
  value = zipmap([module.core.out_vpc_id], [module.core.out_vpc_cidr])
}