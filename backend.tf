terraform {
  backend "s3" {
    bucket = "cloudpatashala-tf-backend"
    key    = "cp-labs/cp-b00/terraform.tfstate"
    region = "ap-south-1"
  }
}
