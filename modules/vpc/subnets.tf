resource "aws_subnet" "cpb00_private_subnet" {
  count = length(var.m_private_subnet_cidr_blocks)

  vpc_id            = aws_vpc.cpb00_main_vpc.id
  cidr_block        = var.m_private_subnet_cidr_blocks[count.index]
  availability_zone = var.m_availability_zones[count.index]

  tags = {
    Name = "${var.m_name_tag}-${var.m_env_tag}-pvt-subnet"
  }
}


resource "aws_subnet" "cpb00_public_subnet" {
  count = length(var.m_public_subnet_cidr_blocks)

  vpc_id            = aws_vpc.cpb00_main_vpc.id
  cidr_block        = var.m_public_subnet_cidr_blocks[count.index]
  availability_zone = var.m_availability_zones[count.index]
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.m_name_tag}-${var.m_env_tag}-pub-subnet"
  }
}