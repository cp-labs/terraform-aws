resource "aws_network_acl" "cpb00_public_nacl" {
  vpc_id = aws_vpc.cpb00_main_vpc.id
  subnet_ids = [aws_subnet.cpb00_public_subnet[0].id, aws_subnet.cpb00_public_subnet[1].id]

  tags = {
    Name = "${var.m_name_tag}-${var.m_env_tag}-pub-nacl"
  }
}

resource "aws_network_acl_rule" "pub_nacl_allow-allport-from-vpc" {
  network_acl_id = aws_network_acl.cpb00_public_nacl.id
  rule_number    = 200
  egress         = false
  protocol       = -1
  rule_action    = "allow"
  cidr_block     = var.vpc_cidr_block
  from_port      = 0
  to_port        = 0
}

resource "aws_network_acl_rule" "pub_nacl_allow_ephermeral_ports" {
  network_acl_id = aws_network_acl.cpb00_public_nacl.id
  rule_number    = 201
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}

resource "aws_network_acl_rule" "pub_nacl_allow_port22-from-internet" {
  network_acl_id = aws_network_acl.cpb00_public_nacl.id
  rule_number    = 300
  egress         = false
  protocol       = -1
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 0
  to_port        = 0
}

resource "aws_network_acl_rule" "pub_nacl_allow_allport-to-vpc" {
  network_acl_id = aws_network_acl.cpb00_public_nacl.id
  rule_number    = 200
  egress         = true
  protocol       = -1
  rule_action    = "allow"
  cidr_block     = var.vpc_cidr_block
  from_port      = 0
  to_port        = 0
}

resource "aws_network_acl_rule" "pub_nacl_allow_allport-pall-to-internet" {
  network_acl_id = aws_network_acl.cpb00_public_nacl.id
  rule_number    = 250
  egress         = true
  protocol       = -1
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 0
  to_port        = 0
}
