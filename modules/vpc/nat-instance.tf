data "aws_ami" "data_amamzon_linux2_nat_instance" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn-ami-vpc-nat-*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["137112412989"] # Amazon
}


resource "aws_security_group" "cpb00_nat_instance_sg" {
  name        = "nat-instance"
  description = "nat instance Sever SG"
  vpc_id      = aws_vpc.cpb00_main_vpc.id

  tags = {
    Terraform   = "true"
    Environment = "demo"
	Batch = "cp-b02"
  Name = "${var.m_name_tag}-${var.m_env_tag}-nat-sg"
  }
}

resource "aws_security_group_rule" "nat_instance_allow_all_inbound" {
  security_group_id = aws_security_group.cpb00_nat_instance_sg.id
  type        = "ingress"
  from_port   = 0
  to_port     = 0
  protocol    = -1
  description = "nat instance SSH"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "nat_instance_allow_all_outbound" {
  security_group_id = aws_security_group.cpb00_nat_instance_sg.id
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = -1
  description = "ALL Outbound Traffic"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_instance" "cpb00_nat_instances" {
	count = length(var.m_public_subnet_cidr_blocks)
  ami                         = data.aws_ami.data_amamzon_linux2_nat_instance.id
  availability_zone           = var.m_availability_zones[count.index]
  ebs_optimized               = var.m_nat_instance_ebs_optimized
  instance_type               = var.m_nat_instance_instance_type
  key_name                    = var.m_key_name
  monitoring                  = false
  subnet_id                   = aws_subnet.cpb00_public_subnet[count.index].id
  vpc_security_group_ids = [aws_security_group.cpb00_nat_instance_sg.id]
  source_dest_check = false
  
  tags = {
  Name = "${var.m_name_tag}-${var.m_env_tag}-nat-${count.index}"
  }
}
