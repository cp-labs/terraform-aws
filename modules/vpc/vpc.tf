resource "aws_vpc" "cpb00_main_vpc" {
  cidr_block           = var.vpc_cidr_block
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "${var.m_name_tag}-${var.m_env_tag}-vpc"
  }
}

output "out_vpc_id" {
  value = aws_vpc.cpb00_main_vpc.id
}

output "out_vpc_cidr" {
  value = aws_vpc.cpb00_main_vpc.cidr_block
}