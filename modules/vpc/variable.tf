variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "m_name_tag" {
  type    = string
  default = "cp-b00"
}

variable "m_env_tag" {
  type    = string
  default = "lab"
}


variable "m_private_subnet_cidr_blocks" {
  type        = list(any)
  default     = ["10.0.1.0/24", "10.0.3.0/24"]
  description = "list of CIDR for Private subnets."
}

variable "m_public_subnet_cidr_blocks" {
  type        = list(any)
  default     = ["10.0.2.0/24", "10.0.4.0/24"]
  description = "list of CIDR for Public subnets."
}

variable "m_availability_zones" {
  default     = ["ap-south-1a", "ap-south-1b"]
  type        = list(any)
  description = "List of availability zones"
}

variable "m_nat_instance_ebs_optimized" {
  default     = false
  type        = bool
  description = "If true, the bastion instance will be EBS-optimized"
}


variable "m_nat_instance_instance_type" {
  default     = "t3.micro"
  type        = string
  description = "Instance type for bastion instance"
}

variable "m_key_name" {
  type        = string
  default = "cp-b02"
  description = "EC2 Key pair name for the bastion"
}
