resource "aws_internet_gateway" "cpb00_main_igw" {
  vpc_id = aws_vpc.cpb00_main_vpc.id

  tags = {
    Name = "${var.m_name_tag}-${var.m_env_tag}-igw"
  }
}