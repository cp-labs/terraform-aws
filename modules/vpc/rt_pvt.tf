resource "aws_route_table" "cpb00_private_route_table" {
  count = length(var.m_private_subnet_cidr_blocks)

  vpc_id = aws_vpc.cpb00_main_vpc.id

  tags = {
    Name = "${var.m_name_tag}-${var.m_env_tag}-pvt-rt"
  }
}

resource "aws_route" "cpb00_private_route_table_rule1" {
  count = length(var.m_private_subnet_cidr_blocks)

  route_table_id         = aws_route_table.cpb00_private_route_table[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  network_interface_id    = aws_instance.cpb00_nat_instances[count.index].primary_network_interface_id
}



resource "aws_route_table_association" "cpb00_private_route_table_association" {
  count = length(var.m_private_subnet_cidr_blocks)

  subnet_id      = aws_subnet.cpb00_private_subnet[count.index].id
  route_table_id = aws_route_table.cpb00_private_route_table[count.index].id
}