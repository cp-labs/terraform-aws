resource "aws_route_table" "cpb00_public_route_table" {
  vpc_id = aws_vpc.cpb00_main_vpc.id

  tags = {
    Name = "${var.m_name_tag}-${var.m_env_tag}-pub-rt"
  }
}

resource "aws_route" "cpb00_public_route_table_rule" {
  route_table_id         = aws_route_table.cpb00_public_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.cpb00_main_igw.id
}

resource "aws_route_table_association" "cpb00_public_route_table_association" {
  count = length(var.m_public_subnet_cidr_blocks)

  subnet_id      = aws_subnet.cpb00_public_subnet[count.index].id
  route_table_id = aws_route_table.cpb00_public_route_table.id
}