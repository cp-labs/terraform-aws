module "core" {
  source = "./modules/vpc"

  m_name_tag = var.name_tag
  m_env_tag  = var.env_tag
  m_key_name = var.key_name
}